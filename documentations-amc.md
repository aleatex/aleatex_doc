# Documentations - AMC 

Le site [aleaTeX](https://aleatex.mathslozano.fr) permet de créer un pré-document pour [AMC](https://www.auto-multiple-choice.net/index.fr) à partir d'une sélection d'atomes à données aléatoires.

:::info
**Tous les exercices d'aleaTeX** sont d'office utilisables avec la sortie AMC au format **AMCOpen**.
Toutefois, il appartient à l'utilisateur final de prévoir la place pour la rédaction des réponses.

Certains exercices proposent d'autres formats de réponse comme :
* des cases à noircir.
* des QCM classiques.
:::
<!-- <video controls="controls" src="/videos/*.mp4" /> -->

## Sélectionner des atomes
<video controls="controls" src="/videos/amcSelectionAtomes.mp4" />

## Paramétrer la mise en page
<video controls="controls" src="/videos/amcParametrerMiseEnPage.mp4" />

## Lancer la pré-compilation AMC
:::warning
Sur **aleaTeX** la compilation est lancée avec deux élèves factices. Malgré tout, le temps de compilation peut être assez long. Soyez patients.
:::
<video controls="controls" src="/videos/amcPreCompilationEnLigne.mp4" />

## Télécharger l'archive 
<video controls="controls" src="/videos/amcTelechargerArchive.mp4" />

## Modifier la liste des élèves
<video controls="controls" src="/videos/amcModifierListeEleves.mp4" />

## Compiler localement avec AMC
<video controls="controls" src="/videos/amcCompilerAvecAMC.mp4" />


## Faire le contrôle :)
