# Documentations - CAN

Le site [aleaTeX](https://aleatex.mathslozano.fr) permet de générer des sujets d'entraînement au concours [La course aux nombres](https://codimd.apps.education.fr/s/7G91CuKJR#) à partir d'une sélection d'atomes à données aléatoires.

<!-- :::warning
Tous les exercices ne sont pas utilisables avec la sortie **PixelArt**, un tag <Badge type="info" text="No PixelArt" /> l'indique.
::: -->

## Constituer son panier
### Quelques atomes CAN

<video controls="controls" src="/videos/canPanierQuelquesAtomes.mp4" />

### CAN complète

<video controls="controls" src="/videos/canPanierCanComplete.mp4" />

## Paramétrer la mise en page

<!-- :::info
Par défaut, la sortie PixelArt choisit un motif compatible avec le nombre de questions des atomes sélectionnés. Un message d'alerte s'affiche s'il n'en existe pas. Il convient de d'abord que la sélection contienne le bon nombre de questions avant de paramétrer la mise en page.
::: -->

<video controls="controls" src="/videos/canParametrerMiseEnPage.mp4" />

## Lancer la compilation CAN

<video controls="controls" src="/videos/canLancerCompilation.mp4" />

## Modifier les paramètres

<video controls="controls" src="/videos/canModifierParametres.mp4" />

## Récupérer le PDF

<video controls="controls" src="/videos/canRecupererPDF.mp4" />

:::info
Une nouvelle compilation permetra de récupérer un PDF avec de nouvelles données.
:::

## Générer plusieurs versions

<video controls="controls" src="/videos/canPlusieursVersions.mp4" />

## Une présentation Beamer pour la classe
:::info
Grâce au paquet [ProfMaquette](https://ctan.org/pkg/profmaquette) de Christophe POULAIN, [aleaTeX](https://aleatex.mathslozano.fr/) dispose d'une maquette Beamer facilitant la projection d'une CAN à l'aide du navigateur [FIREFOX](https://www.mozilla.org/en-US/firefox/new/).

D'autres navigateurs le permettent mais n'ont pas été testés.
:::

<video controls="controls" src="/videos/canBeamerLancerPresentationFirefox.mp4" />