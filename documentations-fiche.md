# Documentations - Fiche

Le site [aleaTeX](https://aleatex.mathslozano.fr) permet de générer des fiches de travail à partir d'une sélection d'atomes à données aléatoires.

<!-- :::warning
Tous les exercices ne sont pas utilisables avec la sortie **PixelArt**, un tag <Badge type="info" text="No PixelArt" /> l'indique.
::: -->

## Constituer son panier

<video controls="controls" src="/videos/panierQuelquesAtomes.mp4" />

## Paramétrer la mise en page

<!-- :::info
Par défaut, la sortie PixelArt choisit un motif compatible avec le nombre de questions des atomes sélectionnés. Un message d'alerte s'affiche s'il n'en existe pas. Il convient de d'abord que la sélection contienne le bon nombre de questions avant de paramétrer la mise en page.
::: -->

<video controls="controls" src="/videos/ficheParametrerMiseEnPage.mp4" />

## Lancer la compilation CAN

<video controls="controls" src="/videos/ficheLancerCompilation.mp4" />

## Récupérer le PDF

<video controls="controls" src="/videos/ficheRecupererPDF.mp4" />

## Modifier les paramètres

<video controls="controls" src="/videos/ficheModifierParametres.mp4" />

## Modifier son panier

<video controls="controls" src="/videos/ficheModifierSonPanier.mp4" />

