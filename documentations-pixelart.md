# Documentations - PixelArt

Le site [aleaTeX](https://aleatex.mathslozano.fr) permet de générer une fiche d'exercices avec un motif PixelArt à colorier à partir d'une sélection d'atomes à données aléatoires.

:::warning
Tous les exercices ne sont pas utilisables avec la sortie **PixelArt**, un tag <Badge type="info" text="No PixelArt" /> l'indique.
:::

## Sélectionner des atomes

<video controls="controls" src="/videos/pixelartSelectionAtomes.mp4" />

## Paramétrer la mise en page

:::info
Par défaut, la sortie PixelArt choisit un motif compatible avec le nombre de questions des atomes sélectionnés. Un message d'alerte s'affiche s'il n'en existe pas. Il convient de d'abord que la sélection contienne le bon nombre de questions avant de paramétrer la mise en page.
:::

<video controls="controls" src="/videos/pixelartParametrerMiseEnPage.mp4" />

## Lancer la compilation PixelArt

<video controls="controls" src="/videos/pixelartLancerCompilation.mp4" />

## Adpapter les paramètres

<video controls="controls" src="/videos/pixelartAdapterParametres.mp4" />

## Récupérer le PDF

<video controls="controls" src="/videos/pixelartRecupererPDF.mp4" />

## Modifier le motif PixelArt

<video controls="controls" src="/videos/pixelartModifierMotif.mp4" />

## Générer plusieurs versions

<video controls="controls" src="/videos/pixelartPlusieursVersions.mp4" />