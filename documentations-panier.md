# Documentations - Panier

Le site [aleaTeX](https://aleatex.mathslozano.fr) permet de faire son petit marché en sélectionnant des atomes à données aléatoires.

<!-- :::warning
Tous les exercices ne sont pas utilisables avec la sortie **PixelArt**, un tag <Badge type="info" text="No PixelArt" /> l'indique.
::: -->

## Constituer son panier

<video controls="controls" src="/videos/panierQuelquesAtomes.mp4" />

## Paramétrer la mise en page

<!-- :::info
Par défaut, la sortie PixelArt choisit un motif compatible avec le nombre de questions des atomes sélectionnés. Un message d'alerte s'affiche s'il n'en existe pas. Il convient de d'abord que la sélection contienne le bon nombre de questions avant de paramétrer la mise en page.
::: -->

<video controls="controls" src="/videos/panierParametrerMiseEnPage.mp4" />

## Modifier son panier

<video controls="controls" src="/videos/panierModifierSonPanier.mp4" />
