# Documentations - Fraction

Le site [aleaTeX](https://aleatex.mathslozano.fr) permet de générer des représentations de fraction.


## Paramétrer la représentation

<video controls="controls" src="/videos/fractionParametrerLaRepresentation.mp4" />

## Lancer la compilation

<video controls="controls" src="/videos/fractionCompilation.mp4" />

## Récupérer la représentation à compléter

<video controls="controls" src="/videos/fractionRecupererImage.mp4" />

## Récupérer le code LaTeX

<video controls="controls" src="/videos/fractionRecupererCodeLateX.mp4" />

## Obtenir la correction

<video controls="controls" src="/videos/fractionObtenirCorrection.mp4" />

## Modifier la représentation

<video controls="controls" src="/videos/fractionModifierRepresentation.mp4" />
