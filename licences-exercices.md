# Licences - Codes et contenu des exercices
::: info
Le contenu des exercices vient d'horizons variés. Dans la mesure du possible les licences d'origine sont reconduites.
:::

## Concours et examens

### DNB, DNBPro, CRPE, CAN<sup><a style="text-decoration:none" href="#a-CAN">(1)</a></sup>, KANGOUROU

Code LaTeX de **Christophe POULAIN** sous licence [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr).

::: warning
Contenus des exercices de la course aux nombres, rendus aléatoires, issu du site de l'académie de [Strasbourg](https://pedagogie.ac-strasbourg.fr/mathematiques/competitions/course-aux-nombres/) sous licence [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/deed.fr)
:::

### MSF<sup><a style="text-decoration:none" href="#a-MSF">(2)</a></sup>

Code LaTeX de **Christophe POULAIN** et **Sébastien LOZANO** sous licence [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr).

::: warning
Contenus des énoncés du concours Mathématiques Sans Frontières mis gracieusement à disposition. Les auteurs demandent expressément de bien vouloir citer la provenance lorsqu'ils sont diffusés de quelque manière que ce soit. Information sur le site [Mathématiques Sans Frontières](https://maths-msf.site.ac-strasbourg.fr/classification.htm) au sein de l'académie de Strasbourg.
:::

### BAC

Code LaTeX de **Cedric PIERQUET** disponible également sur son site [Bac.TeX](https://bactex.cpierquet.fr/)
sous licence [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.fr)
ou [Etalab 2.0](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf).

## Manuels et cahiers

Exercices des manuels et/ou cahiers [Sésamath](https://manuel.sesamath.net/) sous [licence libre](https://manuel.sesamath.net/?page=faq#licence).

Retranscriptions LaTeX par **Christophe POULAIN** et **Sébastien LOZANO** sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)

::: info
Seuls les énoncés des exercices figurant dans le [cahier de 5e 2024](https://manuel.sesamath.net/numerique/?ouvrage=cm5_2024) ont été retranscrits. Il ne semblait pas judicieux de rendre publiquement accessibles les corrections.

Toutefois ces dernières le sont via un compte Sésaprof.

Sésamath mettra également à disposition de nombreux exercices supplémentaires, non édités par manque de place, dans l’[ouvrage numérique feuilletable](https://manuel.sesamath.net/numerique/?ouvrage=cm5_2024) ainsi qu’en téléchargement ! Nous n'avons pas retranscrits ces exercices.
:::

**( 1 )** : **C**ourse **A**ux **N**ombres{#a-CAN}

**( 2 )** : **M**athématiques **S**ans **F**rontières{#a-MSF}