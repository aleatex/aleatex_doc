# Documentations - Cartes

Le site [aleaTeX](https://aleatex.mathslozano.fr) permet de générer des [cartes de type Magic](https://aleatex.mathslozano.fr/generators/cartes/).

<video controls="controls" src="/videos/cartesCarteModele.mp4" />

## Paramétrer

<video controls="controls" src="/videos/cartesParametrer.mp4" />

## Lancer la compilation

<video controls="controls" src="/videos/cartesCompilation.mp4" />

## Récupérer le document PDF

<video controls="controls" src="/videos/cartesRecupererPDF.mp4" />

## Récupérer le code LaTeX

<video controls="controls" src="/videos/cartesRecupererCodeLaTeX.mp4" />

## Une carte en Landscape

<video controls="controls" src="/videos/cartesCarteLandscape.mp4" />