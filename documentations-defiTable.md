# Documentations - Défi "Table"

Le site [aleaTeX](https://aleatex.mathslozano.fr) permet de générer automatiquement des jeux de type "Messages codés".

Les lettres de l'alphabet sont remplacées par des produits. Les valeurs de ces produits permettent de retrouver les lettres cachées en identifiant deux facteurs dont la multiplication donne ces produits et en utilisant la table de Pythagore fournie avec le message. Par exemple, pour le produit 24, on peut utiliser le fait que 24 est le produit de 6 et de 4 ou bien de 8 et de 3.

Des exemples sont disponibles dans le fascicule Jeux 6 de l'APMEP.

<img src="/images/defiTableRegle.png" alt="Défi Table Règle">

## Paramétrer le jeu

<video controls="controls" src="/videos/defiTableParametrerLeJeu.mp4" />

## Lancer la compilation

<video controls="controls" src="/videos/defiTableCompilation.mp4" />

## Récupérer le document PDF

<video controls="controls" src="/videos/defiTableRecupererImage.mp4" />

## Récupérer le code LaTeX

<video controls="controls" src="/videos/defiTableRecupererCodeLaTeX.mp4" />

## Obtenir la correction

<video controls="controls" src="/videos/defiTableObtenirCorrection.mp4" />
