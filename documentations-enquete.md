# Documentations - Enqête

Le site [aleaTeX](https://aleatex.mathslozano.fr) permet de générer des fiches de travail où un nombre "brigand" est à découvrir à partir d'une sélection d'atomes à données aléatoires.

<!-- :::warning
Tous les exercices ne sont pas utilisables avec la sortie **PixelArt**, un tag <Badge type="info" text="No PixelArt" /> l'indique.
::: -->

## Constituer son panier

<video controls="controls" src="/videos/enquetePanier.mp4" />

## Paramétrer la mise en page

<!-- :::info
Par défaut, la sortie PixelArt choisit un motif compatible avec le nombre de questions des atomes sélectionnés. Un message d'alerte s'affiche s'il n'en existe pas. Il convient de d'abord que la sélection contienne le bon nombre de questions avant de paramétrer la mise en page.
::: -->

<video controls="controls" src="/videos/enqueteParametrerMiseEnPage.mp4" />

## Lancer la compilation "Enquête"

<video controls="controls" src="/videos/enqueteLancerCompilation.mp4" />

## Récupérer le PDF

<video controls="controls" src="/videos/enqueteRecupererPDF.mp4" />

:::info
Une nouvelle compilation permetra de récupérer un PDF avec de nouvelles données.
:::

## Générer plusieurs versions

<video controls="controls" src="/videos/enquetePlusieursVersions.mp4" />
