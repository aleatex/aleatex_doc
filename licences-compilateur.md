# Licences - Compilateur TLLoz

[TLLoz](https://tlnet.mathslozano.fr/) est un compilateur LaTeX en ligne issu d'une version modifiée et adaptée du [TeXLive.net Server](https://texlive.net/) de **David CARLISLE** sous [licence MIT](https://opensource.org/license/mit).

Une étroite coopération avec **Christophe POULAIN** de l'académie de Lille a permis sa mise en place. Merci à lui pour ces précieux conseils de TeXnicien.