# Documentations - QCM

Le site [aleaTeX](https://aleatex.mathslozano.fr) permet de créer un QCM d'entraîment ou pour une évaluation à partir d'une sélection d'atomes à données aléatoires ou non.

## Sélectionner des atomes

<video controls="controls" src="/videos/qcmSelectionAtomes.mp4" />

## Paramétrer et compiler

<video controls="controls" src="/videos/qcmConfigurerCompilerSelectionAtomes.mp4" />

## Récupérer le PDF

<video controls="controls" src="/videos/qcmRecupererPDFSelectionAtomes.mp4" />

## Une seule sélection plusieurs maquettes
Grâce au paquet [ProfMaquette](https://ctan.org/pkg/profmaquette) de Christophe POULAIN, il est possible d'obtenir plusieurs maquettes distinctes à partir de la même sélection d'atomes.

### QCM d'entrainement
Une maquette pour faire des fiches d'entraînement au QCM.

<video controls="controls" src="/videos/qcmMaquetteQcmEntrainement.mp4" />

### QCM d'évaluation
Une maquette pour faire un sujet d'évaluation avec des données distinctes de celles des fiches d'entraînement au QCM.

<video controls="controls" src="/videos/qcmMaquetteQcmEvaluation.mp4" />

### Présentation Beamer
Une maquette pour obtenir un présentation pour la classe.

:::info
Avec [FIREFOX](https://www.mozilla.org/en-US/firefox/new/) vous pouvez lancer cette présentation Beamer directment dans le navigateur.

Il est également possible d'utiliser cette fonctionnalité dans d'autres navigateurs. Ils n'ont pas tous été testés.
:::

<video controls="controls" src="/videos/qcmBeamerLancerPresentationNavigateur.mp4" />

## Recompiler pour obtenir des données différentes

:::info
Avec une sélection d'atomes dont le code intègre nativement l'aléatoire, il suffit de recompiler pour obtenir une présentation avec des données différentes.
:::

<!-- <video controls="controls" src="/videos/qcmBeamerRecompilerSelectionAtomes.mp4" /> -->
