# Licences - PixelArt

C'est le paquet [ProfMaquette](https://ctan.org/pkg/profmaquette) sous le capot d'[aleaTeX](https://aleatex.mathslozano.fr) qui permet sa sortie PixelArt. Une nouvelle fois merci à **Christophe POULAIN**.

# Auteurs des motifs disponibles
## FaceBook - Coin Boulot des profs de maths
:::info
Certains des motifs viennent de partages sur le **Facebook du "CoinBoulot des profs de maths"**. J'en profite pour remercier cet espace. Si les auteurs se reconnaissent, ils sont invités à se manisfester afin que je puisse indiquer leurs noms.
:::

|**Harry Potter**|**Mario**|**Citrouille Sorcière**|
|:---:|:---:|:---:|
|<img src="/pixelArt/FBCBPM_HarryPotter.png" alt="Harry Potter FBCBPM">|<img src="/pixelArt/FBCBPM_Mario.png" alt="Mario FBCBPM">|<img src="/pixelArt/FBCBPM_CitrouilleSorciere.png" alt="Citrouille Sorcière FBCBPM">|

## Christophe POULAIN

|**Joconde**|
|:---:|
|<img src="/pixelArt/CP_Joconde.png" alt="Joconde CP">|



## Cyril LACONELLI

Ses motifs PixelArt proviennent de la [LaTeXiotheque](https://lmdbt.forge.apps.education.fr/latexiotheque/#PixelArt) et sont sous licence [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.fr).

|**Cardinal**|**Astronaute**|**Happy Dog**|**Koala**|
|:---:|:---:|:---:|:---:|
|<img src="/pixelArt/CL_Cardinal.png" alt="Cardinal CL">|<img src="/pixelArt/CL_Astronaute.png" alt="Astronaute CL">|<img src="/pixelArt/CL_HappyDog.png" alt="Happy Dog CL">|<img src="/pixelArt/CL_Koala.png" alt="Koala CL">|

|**Mario**|**Chat Chibi**|**Harry Potter**|**Charlemange**|
|:---:|:---:|:---:|:---:|
|<img src="/pixelArt/CL_Mario.png" alt="Mario CL">|<img src="/pixelArt/CL_ChatChibi.png" alt="Chat Chibi CL">|<img src="/pixelArt/CL_HarryPotter.png" alt="Harry Potter CL">|<img src="/pixelArt/CL_Charlemagne.png" alt="Charlemagne CL">|

|**Angele**|**Citrouille**|**Flamme Olympique**|
|:---:|:---:|:---:|
|<img src="/pixelArt/CL_Angele.png" alt="Angele CL">|<img src="/pixelArt/CL_Citrouille.png" alt="Citrouille CL">|<img src="/pixelArt/CL_FlammeOlympique.png" alt="Flamme Olympique CL">|

## Sébastien LOZANO

|**BB8**|**Yoda**|**C3PO**|**Sabre Laser**|
|:---:|:---:|:---:|:---:|
|<img src="/pixelArt/SL_BB8.png" alt="BB8 SL">|<img src="/pixelArt/SL_Yoda.png" alt="Yoda SL">|<img src="/pixelArt/SL_C3PO.png" alt="C3PO SL">|<img src="/pixelArt/SL_SabreLaser.png" alt="Sabre Laser SL">|

|**X-Wing**|
|:---:|
|<img src="/pixelArt/SL_Xwing.png" alt="X-Wing SL">|
