<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]

<!-- PROJECT LOGO -->
<br />
<a href="https://forge.apps.education.fr/aleatex/aleatex_doc">
  <img src="public/avatars/SL.png" alt="Logo" width="80" height="80">
</a>

<h2>aleaTeX</h2>
<h3>Documentations et Licences </h3>

<p>
  Code du site des <a href="https://aleatex.doc.mathslozano.fr">documetations et licences</a> du site <a href="https://aleatex.mathslozano.fr">aleaTeX</a> 
  <br />  
  <br />
  <a href="https://forge.apps.education.fr/aleatex/aleatex_doc/issues">Report Bug</a>
  ·
  <a href="https://forge.apps.education.fr/aleatex/aleatex_doc/issues">Request Feature</a>
</p>

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- LICENSE -->
## License

Distributed under the CC BY-NC-SA 4.0 License. See [LICENSE](https://forge.apps.education.fr/aleatex/aleatex_doc/-/blob/master/LICENCE.md) for more information.

<!-- MARKDOWN LINKS & IMAGES -->
[stars-shield]: https://img.shields.io/badge/STARS-1-blue
[stars-url]: https://forge.apps.education.fr/aleatex/aleatex_doc/-/starrers
[issues-shield]: https://img.shields.io/badge/ISSUES-0-green
[issues-url]: https://forge.apps.education.fr/aleatex/aleatex_doc/issues
[license-shield]: https://img.shields.io/badge/LICENCE-CC_BY--NC--SA_4.0-d5d8dc
[license-url]: https://forge.apps.education.fr/aleatex/aleatex_doc/-/blob/master/LICENCE.md
