# Documentations - Beamer 

Le site [aleaTeX](https://aleatex.mathslozano.fr) permet de créer une [présentation Beamer](https://fr.wikipedia.org/wiki/Beamer) à partir d'une sélection d'atomes à données aléatoires ou non.

## Sélectionner des atomes

<video controls="controls" src="/videos/beamerSelectionAtomes.mp4" />

## Lancer la compilation Beamer

<video controls="controls" src="/videos/beamerCompilerSelectionAtomes.mp4" />

## Récupérer le PDF

<video controls="controls" src="/videos/beamerRecupererPDFSelectionAtomes.mp4" />

## Lancer la présentation localement

<video controls="controls" src="/videos/beamerLancerPresentationSelectionAtomes.mp4" />

## Lancer la présentation dans firefox

<video controls="controls" src="/videos/beamerLancerPresentationFirefox.mp4" />

## Recompiler pour obtenir des données différentes

:::info
Avec une sélection d'atomes dont le code intègre nativement l'aléatoire, il suffit de recompiler pour obtenir une présentation avec des données différentes.
:::

<!-- <video controls="controls" src="/videos/beamerRecompilerSelectionAtomes.mp4" /> -->
