# Licenses - PixelArt

It is the [ProfMaquette](https://ctan.org/pkg/profmaquette) under the [aleaTeX](https://aleatex.mathslozano.fr) hood which allows its PixelArt output. Once again thanks to **Christophe POULAIN**.

# Authors of the available patterns
## FaceBook - Coin Boulot des profs de maths
:::info
Some of the patterns come from sharing on the **Facebook of "Le CoinBoulot des profs de maths"**. I take this opportunity to thank this space. If the authors recognize themselves, they are invited to come forward so that I can indicate their names.
:::

|**Harry Potter**|**Mario**|**Citrouille Sorcière**|
|:---:|:---:|:---:|
|<img src="/pixelArt/FBCBPM_HarryPotter.png" alt="Harry Potter FBCBPM">|<img src="/pixelArt/FBCBPM_Mario.png" alt="Mario FBCBPM">|<img src="/pixelArt/FBCBPM_CitrouilleSorciere.png" alt="Citrouille Sorcière FBCBPM">|

## Christophe POULAIN

|**Joconde**|
|:---:|
|<img src="/pixelArt/CP_Joconde.png" alt="Joconde CP">|

## Cyril LACONELLI

Its PixelArt patterns come from the [LaTeXiotheque](https://lmdbt.forge.apps.education.fr/latexiotheque/#PixelArt) and are licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.fr).

|**Cardinal**|**Astronaute**|**Happy Dog**|**Koala**|
|:---:|:---:|:---:|:---:|
|<img src="/pixelArt/CL_Cardinal.png" alt="Cardinal CL">|<img src="/pixelArt/CL_Astronaute.png" alt="Astronaute CL">|<img src="/pixelArt/CL_HappyDog.png" alt="Happy Dog CL">|<img src="/pixelArt/CL_Koala.png" alt="Koala CL">|

|**Mario**|**Chat Chibi**|**Harry Potter**|**Charlemange**|
|:---:|:---:|:---:|:---:|
|<img src="/pixelArt/CL_Mario.png" alt="Mario CL">|<img src="/pixelArt/CL_ChatChibi.png" alt="Chat Chibi CL">|<img src="/pixelArt/CL_HarryPotter.png" alt="Harry Potter CL">|<img src="/pixelArt/CL_Charlemagne.png" alt="Charlemagne CL">|

|**Angele**|**Citrouille**|**Flamme Olympique**|
|:---:|:---:|:---:|
|<img src="/pixelArt/CL_Angele.png" alt="Angele CL">|<img src="/pixelArt/CL_Citrouille.png" alt="Citrouille CL">|<img src="/pixelArt/CL_FlammeOlympique.png" alt="Flamme Olympique CL">|

## Sébastien LOZANO

|**BB8**|**Yoda**|**C3PO**|**Sabre Laser**|
|:---:|:---:|:---:|:---:|
|<img src="/pixelArt/SL_BB8.png" alt="BB8 SL">|<img src="/pixelArt/SL_Yoda.png" alt="Yoda SL">|<img src="/pixelArt/SL_C3PO.png" alt="C3PO SL">|<img src="/pixelArt/SL_SabreLaser.png" alt="Sabre Laser SL">|

|**X-Wing**|
|:---:|
|<img src="/pixelArt/SL_Xwing.png" alt="X-Wing SL">|