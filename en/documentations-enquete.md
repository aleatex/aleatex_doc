# Documentations - Investigation

The [aleaTeX](https://aleatex.mathslozano.fr) site allows you to generate worksheets where a "brigand" number is to be discovered from a selection of atoms with random data.

<!-- :::warning
Not all exercises can be used with the **PixelArt** output, a <Badge type="info" text="No PixelArt" /> tag indicates this.
::: -->

## Create your cart

<video controls="controls" src="/videos/enquetePanier.mp4" />

## Set the layout

<!-- :::info
By default, the PixelArt output chooses a pattern compatible with the number of questions of the selected atoms. An alert message is displayed if none exists. It is first necessary that the selection contains the right number of questions before setting the layout.
::: -->

<video controls="controls" src="/videos/enqueteParametrerMiseEnPage.mp4" />

## Launch the "Investigation" compilation

<video controls="controls" src="/videos/enqueteLancerCompilation.mp4" />

## Download the PDF

<video controls="controls" src="/videos/enqueteRecupererPDF.mp4" />

:::info
A new compilation will allow you to retrieve a PDF with new data.
:::

## Generate multiple versions

<video controls="controls" src="/videos/enquetePlusieursVersions.mp4" />