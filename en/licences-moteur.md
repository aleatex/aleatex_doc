# Licenses - Engine
## New version of aleaTeX
::: info
November 2024 - The app gets a makeover. The engine changes.
:::

Developer **Sébastien LOZANO**

## First version of aleaTeX		
Engine out of the Mathalea project under [licence AGPL](https://www.gnu.org/licenses/agpl-3.0.html).

Changes by **Sébastien LOZANO**.

::: info
~~The app is still juvenile, its modified source code will be accessible as soon as it is mature.~~
:::

::: warning
[aleaTeX](https://aleatex.mathslozano.fr) is a dissident project of the Mathalea project from which it diverges from the  [commit ac1df1df](https://forge.apps.education.fr/coopmaths/mathalea/-/commit/ac1df1df13337ba4468a28dbdceda3a9676f9559).

The Mathalea project license can be viewed [here](https://coopmaths.fr/www/mentions_legales/).

For contributors it's [here](https://coopmaths.fr/www/about/).
:::