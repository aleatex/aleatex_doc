<script setup>
import { VPTeamMembers } from 'vitepress/theme'

const members = [
  {
    avatar: '/avatars/SL.png',
    name: 'Sébastien LOZANO',
    title: 'Tinkerer :)',
    links: [
      { icon: 'github', link: 'https://github.com/' },
      { icon: {
          svg: `<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 128 128" >
                <circle
                    style="fill:currentColor;"
                    id="path291"
                    cx="64"
                    cy="64"
                    r="64"
                />
                <g
                    id="g309"
                    transform="matrix(1.2,0,0,1.2,0,0)"
                >
                    <path
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="m 59,34.4 c -3.8,0 -6.9,-3.1 -6.9,-6.9 0,-3.8 3.1,-6.9 6.9,-6.9 3.8,0 6.9,3.1 6.9,6.9 0,3.8 -3.1,6.9 -6.9,6.9 z M 59,40 C 52.1,40 46.5,34.4 46.5,27.5 46.5,20.6 52.1,15 59,15 65.9,15 71.5,20.6 71.5,27.5 71.5,34.4 65.9,40 59,40 Z"
                        id="path4" 
                    />
                    <path                       
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="m 59,97.4 c -3.8,0 -6.9,-3.1 -6.9,-6.9 0,-3.8 3.1,-6.9 6.9,-6.9 3.8,0 6.9,3.1 6.9,6.9 0.1,3.7 -3,6.9 -6.9,6.9 z m 0,5.5 c -6.9,0 -12.5,-5.6 -12.5,-12.5 0,-6.9 5.6,-12.5 12.5,-12.5 6.9,0 12.5,5.6 12.5,12.5 0,6.9 -5.6,12.5 -12.5,12.5 z"
                        id="path6" 
                    />
                    <path
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="m 27.5,52.1 c -3.6,0 -6.9,-3.1 -6.9,-7.4 0,-4.3 3.3,-7.4 6.9,-7.4 3.6,0 6.9,3.1 6.9,7.4 0.1,4.2 -3.2,7.3 -6.9,7.4 z m 0,5.5 C 20.6,57.6 15,51.8 15,44.7 c 0,-7.2 5.6,-13 12.5,-13 6.9,0 12.5,5.8 12.5,12.9 0,7.2 -5.6,13 -12.5,13 z"
                        id="path8"
                    />
                    <path
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="M 64.1,80.7 V 37.2 h -9.3 v 43.5 z"
                        id="path10"
                    />
                    <path
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="m 22.4,59.2 c 0.1,4 0.8,9.4 4,14.2 1.9,2.9 4.9,4.8 7.7,6.2 2.9,1.4 6.1,2.3 9.1,3 3,0.7 5.8,1.2 8,1.5 0.7,0.1 1.4,0.2 2,0.3 0.3,0 0.5,0.1 0.7,0.1 0.6,0.1 0.9,0.1 1,0.1 0.1,0 0.1,0 0.1,0 l 2.3,-8.9 c -0.6,-0.2 -1.4,-0.3 -2.1,-0.4 v 0 c -0.3,0 -0.5,-0.1 -0.8,-0.1 C 53.8,75.1 53.2,75 52.5,74.9 50.4,74.6 47.9,74.2 45.2,73.6 42.6,73 40,72.2 38,71.2 c -2.1,-1 -3.4,-2.1 -3.9,-2.9 -1.8,-2.7 -2.4,-6.2 -2.5,-9.3 0,-1.5 0.1,-2.8 0.2,-3.8 0.1,-0.5 0.1,-0.8 0.1,-1.1 0,-0.1 0,-0.2 0,-0.2 0,0 0,0 0,0 v 0 c 0,0 0,0 -4.5,-0.9 C 23,52 23,52 23,52 v 0 0 0 0 c 0,0 0,0.1 0,0.1 0,0.1 0,0.3 -0.1,0.4 -0.1,0.4 -0.1,0.9 -0.2,1.5 -0.2,1.4 -0.3,3.2 -0.3,5.2 z"
                        id="path12" 
                    />
                </g>
            </svg>`
        },
        link: 'https://forge.apps.education.fr/lozanosebastien',
        // You can include a custom label for accessibility too (optional but recommended):
        ariaLabel: 'forgeEdu link'
      },
      { icon: 'twitter', link: 'https://twitter.com/aleatex54' },
      { icon: 'facebook', link: 'https://www.facebook.com/profile.php?id=61561655135008' }
      
    ]
  },
  {
    avatar: '/avatars/CP.png',
    name: 'Christophe POULAIN',
    title: 'Hobby TeXnician :)',
    links: [
        { icon: 'github', link: 'https://github.com/' },
        { icon: {
          svg: `<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 128 128" >
                <circle
                    style="fill:currentColor;"
                    id="path291"
                    cx="64"
                    cy="64"
                    r="64"
                />
                <g
                    id="g309"
                    transform="matrix(1.2,0,0,1.2,0,0)"
                >
                    <path
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="m 59,34.4 c -3.8,0 -6.9,-3.1 -6.9,-6.9 0,-3.8 3.1,-6.9 6.9,-6.9 3.8,0 6.9,3.1 6.9,6.9 0,3.8 -3.1,6.9 -6.9,6.9 z M 59,40 C 52.1,40 46.5,34.4 46.5,27.5 46.5,20.6 52.1,15 59,15 65.9,15 71.5,20.6 71.5,27.5 71.5,34.4 65.9,40 59,40 Z"
                        id="path4" 
                    />
                    <path                       
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="m 59,97.4 c -3.8,0 -6.9,-3.1 -6.9,-6.9 0,-3.8 3.1,-6.9 6.9,-6.9 3.8,0 6.9,3.1 6.9,6.9 0.1,3.7 -3,6.9 -6.9,6.9 z m 0,5.5 c -6.9,0 -12.5,-5.6 -12.5,-12.5 0,-6.9 5.6,-12.5 12.5,-12.5 6.9,0 12.5,5.6 12.5,12.5 0,6.9 -5.6,12.5 -12.5,12.5 z"
                        id="path6" 
                    />
                    <path
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="m 27.5,52.1 c -3.6,0 -6.9,-3.1 -6.9,-7.4 0,-4.3 3.3,-7.4 6.9,-7.4 3.6,0 6.9,3.1 6.9,7.4 0.1,4.2 -3.2,7.3 -6.9,7.4 z m 0,5.5 C 20.6,57.6 15,51.8 15,44.7 c 0,-7.2 5.6,-13 12.5,-13 6.9,0 12.5,5.8 12.5,12.9 0,7.2 -5.6,13 -12.5,13 z"
                        id="path8"
                    />
                    <path
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="M 64.1,80.7 V 37.2 h -9.3 v 43.5 z"
                        id="path10"
                    />
                    <path
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="m 22.4,59.2 c 0.1,4 0.8,9.4 4,14.2 1.9,2.9 4.9,4.8 7.7,6.2 2.9,1.4 6.1,2.3 9.1,3 3,0.7 5.8,1.2 8,1.5 0.7,0.1 1.4,0.2 2,0.3 0.3,0 0.5,0.1 0.7,0.1 0.6,0.1 0.9,0.1 1,0.1 0.1,0 0.1,0 0.1,0 l 2.3,-8.9 c -0.6,-0.2 -1.4,-0.3 -2.1,-0.4 v 0 c -0.3,0 -0.5,-0.1 -0.8,-0.1 C 53.8,75.1 53.2,75 52.5,74.9 50.4,74.6 47.9,74.2 45.2,73.6 42.6,73 40,72.2 38,71.2 c -2.1,-1 -3.4,-2.1 -3.9,-2.9 -1.8,-2.7 -2.4,-6.2 -2.5,-9.3 0,-1.5 0.1,-2.8 0.2,-3.8 0.1,-0.5 0.1,-0.8 0.1,-1.1 0,-0.1 0,-0.2 0,-0.2 0,0 0,0 0,0 v 0 c 0,0 0,0 -4.5,-0.9 C 23,52 23,52 23,52 v 0 0 0 0 c 0,0 0,0.1 0,0.1 0,0.1 0,0.3 -0.1,0.4 -0.1,0.4 -0.1,0.9 -0.2,1.5 -0.2,1.4 -0.3,3.2 -0.3,5.2 z"
                        id="path12" 
                    />
                </g>
            </svg>`
        },
        link: 'https://forge.apps.education.fr/cpoulain',
        // You can include a custom label for accessibility too (optional but recommended):
        ariaLabel: 'forgeEdu link'
      },
      { icon: 'twitter', link: 'https://twitter.com/' },
      { icon: 'facebook', link: 'https://www.facebook.com/' }
    ]
  },
  {
    avatar: '/avatars/CP2.png',
    name: 'Cédric PIERQUET',
    title: 'TeXnician :)',
    links: [
        { icon: 'github', link: 'https://github.com/' },
        { icon: {
          svg: `<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 128 128" >
                <circle
                    style="fill:currentColor;"
                    id="path291"
                    cx="64"
                    cy="64"
                    r="64"
                />
                <g
                    id="g309"
                    transform="matrix(1.2,0,0,1.2,0,0)"
                >
                    <path
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="m 59,34.4 c -3.8,0 -6.9,-3.1 -6.9,-6.9 0,-3.8 3.1,-6.9 6.9,-6.9 3.8,0 6.9,3.1 6.9,6.9 0,3.8 -3.1,6.9 -6.9,6.9 z M 59,40 C 52.1,40 46.5,34.4 46.5,27.5 46.5,20.6 52.1,15 59,15 65.9,15 71.5,20.6 71.5,27.5 71.5,34.4 65.9,40 59,40 Z"
                        id="path4" 
                    />
                    <path                       
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="m 59,97.4 c -3.8,0 -6.9,-3.1 -6.9,-6.9 0,-3.8 3.1,-6.9 6.9,-6.9 3.8,0 6.9,3.1 6.9,6.9 0.1,3.7 -3,6.9 -6.9,6.9 z m 0,5.5 c -6.9,0 -12.5,-5.6 -12.5,-12.5 0,-6.9 5.6,-12.5 12.5,-12.5 6.9,0 12.5,5.6 12.5,12.5 0,6.9 -5.6,12.5 -12.5,12.5 z"
                        id="path6" 
                    />
                    <path
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="m 27.5,52.1 c -3.6,0 -6.9,-3.1 -6.9,-7.4 0,-4.3 3.3,-7.4 6.9,-7.4 3.6,0 6.9,3.1 6.9,7.4 0.1,4.2 -3.2,7.3 -6.9,7.4 z m 0,5.5 C 20.6,57.6 15,51.8 15,44.7 c 0,-7.2 5.6,-13 12.5,-13 6.9,0 12.5,5.8 12.5,12.9 0,7.2 -5.6,13 -12.5,13 z"
                        id="path8"
                    />
                    <path
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="M 64.1,80.7 V 37.2 h -9.3 v 43.5 z"
                        id="path10"
                    />
                    <path
                        style="fill-rule:evenodd;clip-rule:evenodd;fill:#F17361;"
                        d="m 22.4,59.2 c 0.1,4 0.8,9.4 4,14.2 1.9,2.9 4.9,4.8 7.7,6.2 2.9,1.4 6.1,2.3 9.1,3 3,0.7 5.8,1.2 8,1.5 0.7,0.1 1.4,0.2 2,0.3 0.3,0 0.5,0.1 0.7,0.1 0.6,0.1 0.9,0.1 1,0.1 0.1,0 0.1,0 0.1,0 l 2.3,-8.9 c -0.6,-0.2 -1.4,-0.3 -2.1,-0.4 v 0 c -0.3,0 -0.5,-0.1 -0.8,-0.1 C 53.8,75.1 53.2,75 52.5,74.9 50.4,74.6 47.9,74.2 45.2,73.6 42.6,73 40,72.2 38,71.2 c -2.1,-1 -3.4,-2.1 -3.9,-2.9 -1.8,-2.7 -2.4,-6.2 -2.5,-9.3 0,-1.5 0.1,-2.8 0.2,-3.8 0.1,-0.5 0.1,-0.8 0.1,-1.1 0,-0.1 0,-0.2 0,-0.2 0,0 0,0 0,0 v 0 c 0,0 0,0 -4.5,-0.9 C 23,52 23,52 23,52 v 0 0 0 0 c 0,0 0,0.1 0,0.1 0,0.1 0,0.3 -0.1,0.4 -0.1,0.4 -0.1,0.9 -0.2,1.5 -0.2,1.4 -0.3,3.2 -0.3,5.2 z"
                        id="path12" 
                    />
                </g>
            </svg>`
        },
        link: 'https://forge.apps.education.fr/pierquetcedric',
        // You can include a custom label for accessibility too (optional but recommended):
        ariaLabel: 'forgeEdu link'
      },
      { icon: 'twitter', link: 'https://twitter.com/' },
      { icon: 'facebook', link: 'https://www.facebook.com/' }
    ]
  }    
]
</script>

# The wild team


<VPTeamMembers size="small" :members="members" />