# Documentations - Fraction

The [aleaTeX](https://aleatex.mathslozano.fr) site allows you to generate fraction representations.


## Set the representation

<video controls="controls" src="/videos/fractionParametrerLaRepresentation.mp4" />

## Start compilation

<video controls="controls" src="/videos/fractionCompilation.mp4" />

## Retrieve the representation to complete

<video controls="controls" src="/videos/fractionRecupererImage.mp4" />

## Retrieve the LaTeX code

<video controls="controls" src="/videos/fractionRecupererCodeLateX.mp4" />

## Obtain the correction

<video controls="controls" src="/videos/fractionObtenirCorrection.mp4" />

## Modify the representation

<video controls="controls" src="/videos/fractionModifierRepresentation.mp4" />
