# Documentations - AMC 

The site [aleaTeX](https://aleatex.mathslozano.fr) allows you to create a pre-document for [AMC](https://www.auto-multiple-choice.net/index.fr) from a selection of atoms with random data.

:::info
**All aleaTeX** exercises can automatically be used with the AMC output in **AMCOpen** format.
However, it is up to the end user to provide space for writing responses.

Some exercises offer other response formats such as:
* boxes to blacken.
* classic multiple choice questions.
:::
<!-- <video controls="controls" src="/videos/*.mp4" /> -->

## Select atoms
<video controls="controls" src="/videos/amcSelectionAtomes.mp4" />

## Configure the layout
<video controls="controls" src="/videos/amcParametrerMiseEnPage.mp4" />

## Start AMC pre-compilation
:::warning
Sur **aleaTeX** la compilation est lancée avec deux élèves factices. Malgré tout, le temps de compilation peut être assez long. Soyez patients.
:::
<video controls="controls" src="/videos/amcPreCompilationEnLigne.mp4" />

## Download the archive 
<video controls="controls" src="/videos/amcTelechargerArchive.mp4" />

## Edit student list
<video controls="controls" src="/videos/amcModifierListeEleves.mp4" />

## Compile locally with AMC
<video controls="controls" src="/videos/amcCompilerAvecAMC.mp4" />

## Do the exam :)
