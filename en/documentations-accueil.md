# Documentations
## LaTeX Formatting

Thanks to a real cooperation with **Christophe POULAIN**, via his LaTeX packages [ProfMaquette](https://ctan.org/pkg/profmaquette) and [ProfCollege](https://ctan.org/pkg/profcollege), [aleaTeX](https://aleatex.mathslozano.fr) offers several LaTeX formattings, here are the links to the documentations.

* Formatting [**Beamer**](https://fr.wikipedia.org/wiki/Beamer) - [See the documentation](./documentations-beamer.md)
* Formatting **AMC<sup><a style="text-decoration:none" href="#a-AMC">(2)</a></sup>** - [See the documentation](./documentations-amc.md)
* Formatting **PixelArt** - [See the documentation](./documentations-pixelart.md)
* Formatting **CAN<sup><a style="text-decoration:none" href="#a-CAN">(1)</a></sup>** - [See the documentation](./documentations-can.md)
* Formatting **Fiche** - [See the documentation](./documentations-fiche.md)
* Formatting **FlashCards** - [See the documentation](./documentations-flashcards.md)
* Formatting **CrackCode** - [Voir la documentation](./documentations-crackcode.md)
* Formatting **Investigation** - [Voir la documentation](./documentations-enquete.md)

## Image generators
[aleaTeX](https://aleatex.mathslozano.fr/generators/fraction/) offers generators to obtain LaTeX code and images of various things.
### Fraction
Representation of fractions in various ways. [See the documentation](./documentations-representerUneFraction.md)

### "Table" Challenge

"Table" Challenge type game. [See the documentation](./documentations-defiTable.md)

### Flashcards

Flashcards. [See documentation](./documentations-cartes.md)

<hr>

**( 1 )** : **C**ourse **A**ux **N**ombres{#a-CAN}

**( 2 )** : **A**uto**M**ultiple**C**hoice{#a-AMC}

