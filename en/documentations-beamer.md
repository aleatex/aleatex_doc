# Documentations - Beamer 

The [aleaTeX](https://aleatex.mathslozano.fr) site allows you to create a [Beamer presentation](https://fr.wikipedia.org/wiki/Beamer) from a selection of atoms with random or non-random data.

## Select atoms

<video controls="controls" src="/videos/beamerSelectionAtomes.mp4" />

## Launch the Beamer compilation

<video controls="controls" src="/videos/beamerCompilerSelectionAtomes.mp4" />

## Download PDF

<video controls="controls" src="/videos/beamerRecupererPDFSelectionAtomes.mp4" />

## Launch presentation locally

<video controls="controls" src="/videos/beamerLancerPresentationSelectionAtomes.mp4" />

## Launch the presentation in Firefox

<video controls="controls" src="/videos/beamerLancerPresentationFirefox.mp4" />

## Recompile to get different data

:::info
With a selection of atoms whose code natively integrates randomness, it is enough to recompile to obtain a presentation with different data.
:::

<!-- <video controls="controls" src="/videos/beamerRecompilerSelectionAtomes.mp4" /> -->
