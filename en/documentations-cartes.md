# Documentations - Cartes

The site [aleaTeX](https://aleatex.mathslozano.fr) allows you to generate [Magic type cards](https://aleatex.mathslozano.fr/generators/cartes/).

<video controls="controls" src="/videos/cartesCarteModele.mp4" />

## Configure

<video controls="controls" src="/videos/cartesParametrer.mp4" />

## Start compilation

<video controls="controls" src="/videos/cartesCompilation.mp4" />

## Download the PDF

<video controls="controls" src="/videos/cartesRecupererPDF.mp4" />

## Retrieve the LaTeX code

<video controls="controls" src="/videos/cartesRecupererCodeLaTeX.mp4" />

## A Landscape card

<video controls="controls" src="/videos/cartesCarteLandscape.mp4" />