# Documentations - CAN

The [aleaTeX](https://aleatex.mathslozano.fr) site allows you to generate practice topics for the [La course aux nombres](https://codimd.apps.education.fr/s/7G91CuKJR#) competition from a selection of atoms with random data.

<!-- :::warning
Tous les exercices ne sont pas utilisables avec la sortie **PixelArt**, un tag <Badge type="info" text="No PixelArt" /> l'indique.
::: -->

## Build your cart
### A few CAN atoms

<video controls="controls" src="/videos/canPanierQuelquesAtomes.mp4" />

### A whole CAN

<video controls="controls" src="/videos/canPanierCanComplete.mp4" />

## Set your layout

<!-- :::info
Par défaut, la sortie PixelArt choisit un motif compatible avec le nombre de questions des atomes sélectionnés. Un message d'alerte s'affiche s'il n'en existe pas. Il convient de d'abord que la sélection contienne le bon nombre de questions avant de paramétrer la mise en page.
::: -->

<video controls="controls" src="/videos/canParametrerMiseEnPage.mp4" />

## Launch the CAN compilation

<video controls="controls" src="/videos/canLancerCompilation.mp4" />

## Adjust settings

<video controls="controls" src="/videos/canModifierParametres.mp4" />

## Download PDF

<video controls="controls" src="/videos/canRecupererPDF.mp4" />

:::info
A new compilation will allow you to recover a PDF with new data.
:::

## Generate multiple versions

<video controls="controls" src="/videos/canPlusieursVersions.mp4" />

## A Beamer presentation for the class
:::info
Thanks to the [ProfMaquette](https://ctan.org/pkg/profmaquette) package from Christophe POULAIN, [aleaTeX](https://aleatex.mathslozano.fr/) has a Beamer model facilitating the projection of a CAN using the [FIREFOX](https://www.mozilla.org/en-US/firefox/new/) browser.

Other browsers allow this but have not been tested.
:::

<video controls="controls" src="/videos/canBeamerLancerPresentationFirefox.mp4" />