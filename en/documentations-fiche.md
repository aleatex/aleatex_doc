# Documentations - Fiche

Le site [aleaTeX](https://aleatex.mathslozano.fr) permet de générer des fiches de travail à partir d'une sélection d'atomes à données aléatoires.

<!-- :::warning
Tous les exercices ne sont pas utilisables avec la sortie **PixelArt**, un tag <Badge type="info" text="No PixelArt" /> l'indique.
::: -->

## Build your cart

<video controls="controls" src="/videos/ficheQuelquesAtomes.mp4" />

## Set your layout

<!-- :::info
Par défaut, la sortie PixelArt choisit un motif compatible avec le nombre de questions des atomes sélectionnés. Un message d'alerte s'affiche s'il n'en existe pas. Il convient de d'abord que la sélection contienne le bon nombre de questions avant de paramétrer la mise en page.
::: -->

<video controls="controls" src="/videos/ficheParametrerMiseEnPage.mp4" />

## Launch the Fiche compilation

<video controls="controls" src="/videos/ficheLancerCompilation.mp4" />

## Download PDF

<video controls="controls" src="/videos/ficheRecupererPDF.mp4" />

## Adjust settings

<video controls="controls" src="/videos/ficheModifierParametres.mp4" />

## Edit your cart

<video controls="controls" src="/videos/ficheModifierSonPanier.mp4" />

