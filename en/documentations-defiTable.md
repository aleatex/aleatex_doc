# Documentations - Défi "Table"

The [aleaTeX](https://aleatex.mathslozano.fr) site allows you to automatically generate "Coded Messages" type games.

The letters of the alphabet are replaced by products. The values ​​of these products allow you to find the hidden letters by identifying two factors whose multiplication gives these products and by using the Pythagorean table provided with the message. For example, for the product 24, you can use the fact that 24 is the product of 6 and 4 or 8 and 3.

Examples are available in the APMEP Games 6 booklet.

<img src="/images/defiTableRegle.png" alt="Défi Table Règle">

## Configure the game

<video controls="controls" src="/videos/defiTableParametrerLeJeu.mp4" />

## Start compilation

<video controls="controls" src="/videos/defiTableCompilation.mp4" />

## Download the PDF

<video controls="controls" src="/videos/defiTableRecupererImage.mp4" />

## Retrieve the LaTeX code

<video controls="controls" src="/videos/defiTableRecupererCodeLaTeX.mp4" />

## Obtain the correction

<video controls="controls" src="/videos/defiTableObtenirCorrection.mp4" />
