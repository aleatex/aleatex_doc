# Licenses - Exercise codes and content
::: info
The content of the exercises comes from varied backgrounds. Where possible, the original licenses are renewed.
:::

## Competitions and exams

### DNB, DNBPro, CRPE, CAN<sup><a style="text-decoration:none" href="#a-CAN">(1)</a></sup>, KANGOUROU

LaTeX code from **Christophe POULAIN** under license [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr).

::: warning
Contents of the course aux nombres exercises, made random, from the [Strasbourg](https://pedagogie.ac-strasbourg.fr/mathematiques/competitions/course-aux-nombres/) academy website under license [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/deed.fr)
:::

### MSF<sup><a style="text-decoration:none" href="#a-MSF">(2)</a></sup>

LaTeX code from **Christophe POULAIN** and **Sébastien LOZANO** under license [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr).

::: warning
Contents of the Mathematics Without Borders competition statements made available free of charge. The authors expressly request that provenance be cited when distributed in any manner. Information on the [Mathematics Without Borders website](https://maths-msf.site.ac-strasbourg.fr/classification.htm) within the Strasbourg academy.
:::

### BAC

LaTeX code from **Cedric PIERQUET** also available on his website [Bac.TeX](https://bactex.cpierquet.fr/)
under license [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.fr)
or [Etalab 2.0](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf).

## Manuels et cahiers

Exercises from manuals and/or notebooks [Sésamath](https://manuel.sesamath.net/) under [license libre](https://manuel.sesamath.net/?page=faq#licence).

LaTeX transcriptions by **Christophe POULAIN** and **Sébastien LOZANO** under license [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)

::: info
Only the statements of the exercises appearing in the [5th grade 2024 notebook](https://manuel.sesamath.net/numerique/?ouvrage=cm5_2024) have been transcribed. It did not seem wise to make the corrections publicly available.

However, the latter are via a Sésaprof account.

Sésamath will also make available numerous additional exercises, unedited due to lack of space, in the [leafable digital work](https://manuel.sesamath.net/numerique/?ouvrage=cm5_2024) as well as for download! We have not transcribed these exercises.
:::

**( 1 )** : **C**ourse **A**ux **N**ombres{#a-CAN}

**( 2 )** : **M**athématiques **S**ans **F**rontières{#a-MSF}