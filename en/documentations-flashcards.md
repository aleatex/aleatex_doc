# Documentations - FlashCards

The [aleaTeX](https://aleatex.mathslozano.fr) site allows you to generate flashcards from a selection of atoms with random data.

:::warning
Automation has its limits, the code of some exercises may need to be retouched directly in the editing window in order to adjust the PDF output.
:::

## Create your cart

<video controls="controls" src="/videos/flashcardsPanier.mp4" />

## Set the layout

<!-- :::info
By default, the PixelArt output chooses a pattern compatible with the number of questions of the selected atoms. An alert message is displayed if one does not exist. It is advisable to first ensure that the selection contains the correct number of questions before setting the layout.
::: -->

<video controls="controls" src="/videos/flashcardsParametrer.mp4" />

## Launch the Cards compilation

<video controls="controls" src="/videos/flashcardsLancerCompilation.mp4" />

## Retrieve the PDF

<video controls="controls" src="/videos/flashcardsRecupererPDF.mp4" />

## Interest of the canvas configuration

<video controls="controls" src="/videos/flashcardsCanevas.mp4" />
