# Documentations - Cart

The site [aleaTeX](https://aleatex.mathslozano.fr) allows you to do your shopping by selecting atoms with random data.

<!-- :::warning
Tous les exercices ne sont pas utilisables avec la sortie **PixelArt**, un tag <Badge type="info" text="No PixelArt" /> l'indique.
::: -->

## Build your cart

<video controls="controls" src="/videos/panierQuelquesAtomes.mp4" />

## Set your layout

<!-- :::info
Par défaut, la sortie PixelArt choisit un motif compatible avec le nombre de questions des atomes sélectionnés. Un message d'alerte s'affiche s'il n'en existe pas. Il convient de d'abord que la sélection contienne le bon nombre de questions avant de paramétrer la mise en page.
::: -->

<video controls="controls" src="/videos/panierParametrerMiseEnPage.mp4" />

## Edit your cart

<video controls="controls" src="/videos/panierModifierSonPanier.mp4" />
