---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "aleaTeX"
  text: "Documentation"
  tagline: Documentations and licenses
  image:
    src: /dark/hero-img-color.svg
    alt: aleaTeX
  actions:
    - theme: brand
      text: Documentations
      link: /en/documentations-accueil
    - theme: alt
      text: Licenses
      link: /en/licences-accueil

features:
  - title: aleaTeX
    icon:
      light: /light/latex.svg
      dark: /dark/latex-color.svg
      alt: code icon
    details: Random data exercises. A natively random LaTeX export. With an up-to-date local LaTeX distribution and a magical ProfCollege and ProfMaquette combo.
    link: https://aleatex.mathslozano.fr
  - title: Beamer
    icon:
      light: /light/beamer.svg
      dark: /dark/beamer-color.svg
      alt: code icon
    link: ./documentations-beamer
    details: The aleaTeX site allows you to create a Beamer presentation from a selection of atoms with random data.
  - title: AMC
    icon:
      light: /light/amc.svg
      dark: /dark/amc-color.svg
      alt: code icon
    link: ./documentations-amc
    details: The site aleaTeX allows you to create a pre-document for AMC from a selection of atoms with random data.
  - title: PixelArt
    icon:
      light: /light/pixelArt-heart.svg
      dark: /dark/pixelArt-heart-color.svg
      alt: code icon
    link: ./documentations-pixelart
    details: The aleaTeX site allows you to generate an exercise sheet with a PixelArt pattern to color from a selection of atoms with random data.
---

