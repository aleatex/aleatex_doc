# Licenses

Several projects have enabled the existence of [aleaTeX](https://aleatex.mathslozano.fr) but above all a real cooperation with **Christophe POULAIN**, notably father of the LaTeX packages [ProfMaquette](https://ctan.org/pkg/profmaquette) and [ProfCollege](https://ctan.org/pkg/profcollege).

All information regarding licenses:
* For the [engine](./licences-moteur.md)
* For the [TLLoz](./licences-compilateur.md) compiler
* For [exercises](./licences-exercices.md)
* For [pixelArt](./licences-pixelart.md)





