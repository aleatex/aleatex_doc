# Licenses - TLLoz Compiler

[TLLoz](https://tlnet.mathslozano.fr/) is an online LaTeX compiler out of a modified and adapted version of **David CARLISLE**'s [TeXLive.net Server](https://texlive.net/) under [license MIT](https://opensource.org/license/mit).

Close cooperation with **Christophe POULAIN** from the Lille academy enabled its implementation. Thanks to him for these valuable TeXnician tips.