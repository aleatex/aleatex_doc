# Documentations - QCM

The site [aleaTeX](https://aleatex.mathslozano.fr) allows you to create a training multiple choice or for an evaluation from a selection of atoms with random or non-random data.

## Select atoms

<video controls="controls" src="/videos/qcmSelectionAtomes.mp4" />

## Configure and compile

<video controls="controls" src="/videos/qcmConfigurerCompilerSelectionAtomes.mp4" />

## Download PDF

<video controls="controls" src="/videos/qcmRecupererPDFSelectionAtomes.mp4" />

## A single selection several models
Thanks to the [ProfMaquette](https://ctan.org/pkg/profmaquette) package from Christophe POULAIN, it is possible to obtain several distinct models from the same selection of atoms.

### Training MCQs
A model for making MCQ practice sheets.

<video controls="controls" src="/videos/qcmMaquetteQcmEntrainement.mp4" />

### Assessment MCQ
A model to make an assessment subject with data distinct from that of the MCQ training sheets.

<video controls="controls" src="/videos/qcmMaquetteQcmEvaluation.mp4" />

### Beamer presentation
A model to obtain a presentation for the class.

:::info
With [FIREFOX](https://www.mozilla.org/en-US/firefox/new/) you can launch this Beamer presentation directly in the browser.

It is also possible to use this feature in other browsers. Not all of them have been tested.
:::

<video controls="controls" src="/videos/qcmBeamerLancerPresentationNavigateur.mp4" />

## Recompile to get different data

:::info
With a selection of atoms whose code natively integrates randomness, simply recompile to obtain a presentation with different data.
:::

<!-- <video controls="controls" src="/videos/beamerRecompilerSelectionAtomes.mp4" /> -->
