# Documentations - PixelArt

The [aleaTeX](https://aleatex.mathslozano.fr) site allows you to generate an exercise sheet with a PixelArt pattern to color from a selection of atoms with random data.

:::warning
Not all exercises are usable with **PixelArt** output, a <Badge type="info" text="No PixelArt" /> badge indicates this.
:::

## Select atoms

<video controls="controls" src="/videos/pixelartSelectionAtomes.mp4" />

## Configure the layout

:::info
By default, the PixelArt output chooses a pattern compatible with the number of questions of the selected atoms. An alert message is displayed if one does not exist. It is advisable to first make sure that the selection contains the correct number of questions before setting the layout.
:::

<video controls="controls" src="/videos/pixelartParametrerMiseEnPage.mp4" />

## Launch the PixelArt compilation

<video controls="controls" src="/videos/pixelartLancerCompilation.mp4" />

## Adjust settings

<video controls="controls" src="/videos/pixelartAdapterParametres.mp4" />

## Download PDF

<video controls="controls" src="/videos/pixelartRecupererPDF.mp4" />

## Change PixelArt pattern

<video controls="controls" src="/videos/pixelartModifierMotif.mp4" />

## Generate multiple versions

<video controls="controls" src="/videos/pixelartPlusieursVersions.mp4" />
