# Documentations
## Mises en formes LaTeX

Grâce à une réelle coopération avec **Christophe POULAIN**, via ses paquets LaTeX [ProfMaquette](https://ctan.org/pkg/profmaquette) et [ProfCollege](https://ctan.org/pkg/profcollege), [aleaTeX](https://aleatex.mathslozano.fr) propose plusieurs mises en forme LaTeX, dont voici les liens vers les documentations.

* Mise en forme [**Beamer**](https://fr.wikipedia.org/wiki/Beamer) - [Voir la documentation](./documentations-beamer.md)
* Mise en forme **AMC<sup><a style="text-decoration:none" href="#a-AMC">(2)</a></sup>** - [Voir la documentation](./documentations-amc.md)
* Mise en forme **PixelArt** - [Voir la documentation](./documentations-pixelart.md)
* Mise en forme **CAN<sup><a style="text-decoration:none" href="#a-CAN">(1)</a></sup>** - [Voir la documentation](./documentations-can.md)
* Mise en forme **Fiche** - [Voir la documentation](./documentations-fiche.md)
* Mise en forme **FlashCards** - [Voir la documentation](./documentations-flashcards.md)
* Mise en forme **CrackCode** - [Voir la documentation](./documentations-crackcode.md)
* Mise en forme **Enquete** - [Voir la documentation](./documentations-enquete.md)

## Générateurs d'images
[aleaTeX](https://aleatex.mathslozano.fr/generators/fraction/) propose des générateurs permettant d'obtenir le code LaTeX ainsi que des images de diverses choses.
### Fraction
Représentation des fractions de diverses façons. [Voir la documentation](./documentations-representerUneFraction.md)

### Défi "Table"

Jeu de type Défi "Table". [Voir la documentation](./documentations-defiTable.md)

### Cartes Mémo

Cartes memo. [Voir la documentation](./documentations-cartes.md)

<hr>

**( 1 )** : **C**ourse **A**ux **N**ombres{#a-CAN}

**( 2 )** : **A**uto**M**ultiple**C**hoice{#a-AMC}