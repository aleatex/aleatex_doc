# Licences - Moteur 
## Nouvelle version d'aleaTeX
::: info
Novembre 2024 - L'application fait peau neuve. Le moteur change.
:::

Développeur **Sébastien LOZANO**

## Première version d'aleaTeX		
Moteur issu de celui du projet Mathalea sous [licence AGPL](https://www.gnu.org/licenses/agpl-3.0.html).

Modifications par **Sébastien LOZANO**.

::: info
~~L'application étant encore juvénile, son code source modifié sera accessible dès qu'elle sera à maturité.~~
:::

::: warning
[aleaTeX](https://aleatex.mathslozano.fr) est un projet dissident du projet Mathalea dont il diverge à partir du [commit ac1df1df](https://forge.apps.education.fr/coopmaths/mathalea/-/commit/ac1df1df13337ba4468a28dbdceda3a9676f9559).

La licence du projet Mathalea consultable [là](https://coopmaths.fr/www/mentions_legales/).

Pour les contributeurs c'est [ici](https://coopmaths.fr/www/about/).
:::