# Licences

Plusieurs projets ont permis l'existence d'[aleaTeX](https://aleatex.mathslozano.fr) mais surtout une réelle coopération avec **Christophe POULAIN** notamment père des paquets LaTeX [ProfMaquette](https://ctan.org/pkg/profmaquette) et [ProfCollege](https://ctan.org/pkg/profcollege).

L'ensemble des informations concernant les licences :
* Pour le [moteur](./licences-moteur.md)
* Pour le compilateur [TLLoz](./licences-compilateur.md)
* Pour les [exercices](./licences-exercices.md)
* Pour le [pixelArt](./licences-pixelart.md)





