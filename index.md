---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "aleaTeX"
  text: "Documentation"
  tagline: Documentations et licences
  image:
    src: /dark/hero-img-color.svg
    alt: aleaTeX
  actions:
    - theme: brand
      text: Documentations
      link: /documentations-accueil
    - theme: alt
      text: Licences
      link: /licences-accueil

features:
  - title: aleaTeX
    icon:
      light: /light/latex.svg
      dark: /dark/latex-color.svg
      alt: code icon
    details: Des exercices à données aléatoires. Un export LaTeX nativement aléatoire. Avec une distribution LaTeX locale à jour et un combo magique ProfCollege et ProfMaquette.
    link: https://aleatex.mathslozano.fr
  - title: Beamer
    icon:
      light: /light/beamer.svg
      dark: /dark/beamer-color.svg
      alt: code icon
    link: ./documentations-beamer
    details: Le site aleaTeX permet de créer une présentation Beamer à partir d'une sélection d'atomes à données aléatoires.
  - title: AMC
    icon:
      light: /light/amc.svg
      dark: /dark/amc-color.svg
      alt: code icon
    link: ./documentations-amc
    details: Le site aleaTeX permet de créer un pré-document pour AMC à partir d'une sélection d'atomes à données aléatoires.
  - title: PixelArt
    icon:
      light: /light/pixelArt-heart.svg
      dark: /dark/pixelArt-heart-color.svg
      alt: code icon
    link: ./documentations-pixelart
    details: Le site aleaTeX permet de générer une fiche d'exercices avec un motif PixelArt à colorier à partir d'une sélection d'atomes à données aléatoires.
---

