# Documentations - FlashCards

Le site [aleaTeX](https://aleatex.mathslozano.fr) permet de générer des flashcards à partir d'une sélection d'atomes à données aléatoires.

:::warning
L'automatisation ayant ses limites, le code de certains exercices peut nécessiter d'être retoucher directement dans la fenêtre d'édition afin d'ajuster la sortie PDF.
:::

## Constituer son panier

<video controls="controls" src="/videos/flashcardsPanier.mp4" />

## Paramétrer la mise en page

<!-- :::info
Par défaut, la sortie PixelArt choisit un motif compatible avec le nombre de questions des atomes sélectionnés. Un message d'alerte s'affiche s'il n'en existe pas. Il convient de d'abord que la sélection contienne le bon nombre de questions avant de paramétrer la mise en page.
::: -->

<video controls="controls" src="/videos/flashcardsParametrer.mp4" />

## Lancer la compilation Cartes

<video controls="controls" src="/videos/flashcardsLancerCompilation.mp4" />

## Récupérer le PDF

<video controls="controls" src="/videos/flashcardsRecupererPDF.mp4" />

## Intérêt du paramétrage du canevas

<video controls="controls" src="/videos/flashcardsCanevas.mp4" />
